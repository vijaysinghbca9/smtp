<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mails extends Model
{
    protected $fillable = ['created_by', 'subject', 'mail_text'];
    protected $table = 'mails';
    
    public function to() {
        return $this->hasMany('App\To', 'mail_id');
    }
}
