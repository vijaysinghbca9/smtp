<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class To extends Model
{
    protected $fillable = ['user_id', 'mail_id'];
    protected $table = 'to';
    
    public function users() {
        return $this->hasMany('App\User', 'id', 'user_id');
    }
}
