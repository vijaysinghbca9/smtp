<?php
namespace App\Http\Controllers;

use App\Mails;
use App\Inbox;
use App\To;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Sent;

class ComposeMailController extends Controller
{

    public function sendMail(Request $request)
    {
        try {
            
            $rules = array(
                'to' => 'required'
            );
            
            $messages = array(
                'to.required' => 'recipient email address is required.'
            );
            
            $validator = \Validator::make(array(
                'to' => $request['to']
            ), $rules, $messages);
            
            if (!$validator->fails()) {
                
                $currentUser = User::where('email', $request->from)->first();
                
                $emails = explode(';', $request['to']);
                
                foreach ($emails as $email) {
                    
                    $rules = array(
                        'email' => 'exists:users,email'
                    );
                    
                    $messages = array(
                        'exists' => $email . ' email address does not exist.'
                    );
                    
                    $validator = \Validator::make(array(
                        'email' => $email
                    ), $rules, $messages);
                                        
                    if ($validator->fails()) {
                        
                        $errors = $validator->errors();
                        return response()->json($errors->all());
                    }
                }
                
                $createdMail = Mails::create(array(
                    'subject' => $request['subject'],
                    'mail_text' => $request['mail_text'],
                    'created_by' => $currentUser->id
                ));
                
                if ($createdMail) {
                    
                    foreach ($emails as $email) {
                        
                        $user = User::where('email', trim(str_replace(' ', '', $email)))->first();
                        $inboxEntry = Inbox::create(array(
                            'user_id' => $user->id,
                            'from_user_id' => $currentUser->id,
                            'mail_id' => $createdMail->id,
                            'is_trashed' => 0,
                            'is_unread' => 1
                        ));
                        
                        if ($inboxEntry) {
                            
                            $sentEntry = Sent::create(array(
                                'user_id' => $currentUser->id,
                                'to_user_id' => $user->id,
                                'mail_id' => $createdMail->id,
                                'is_trashed' => 0,
                                'is_unread' => 0
                            ));
                            
                            if ($sentEntry) {
                                
                                $toEntry = To::create(array(
                                    'user_id' => $user->id,
                                    'mail_id' => $createdMail->id
                                ));
                            }
                        }
                    }
                }
                
                return response()->json(array('Mail sent'));
                
            } else {
                
                $errors = $validator->errors();
                return response()->json($errors->all());
            }
        } catch (\Exception $ex) {
            
            return response()->json(array($ex->getMessage()));
        }
    }
}