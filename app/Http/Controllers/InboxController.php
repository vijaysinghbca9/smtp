<?php

namespace App\Http\Controllers;

use App\Inbox;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InboxController extends Controller
{
    /**
     * Get all mails from inbox for requested user
     * @param Request $request
     */
    public function getAllMails(Request $request)
    {
        try {
            /**
             * Validate request for mandatory parameters
             * @var array $rules
             */
            $rules = array(
                'email' => 'required'
            );
            
            $messages = array(
                'email.required' => 'Your email address is required.'
            );
            
            $validator = \Validator::make(array(
                'email' => $request['email']
            ), $rules, $messages);
            
            if (!$validator->fails()) {
                
                /**
                 * Get current user details
                 * @var User $currentUser
                 */
                $currentUser = User::where('email', $request->email)->first();
                if (!$currentUser) {
                    
                    return response()->json(array('Invalid loggedin user.'));
                }
                
                /**
                 * Get all mails from inbox for current user
                 * @var Inbox $allMails
                 */
                //$allMails = Inbox::where('user_id', $currentUser->id)
                //->where('is_trashed', 0)->with('mails')->with('to')->with('from')->get();
                
                $allMails = DB::table('inbox')
                ->where(['inbox.is_trashed'=>0, 'inbox.user_id'=>$currentUser->id])
                ->join('mails','mails.id','=','inbox.mail_id')
                ->join('users as u1','u1.id','=','inbox.user_id')
                ->join('users as u2','u2.id','=','mails.created_by')
                ->get(['u1.email as to', 'u2.email as from', 'inbox.id', 'mails.subject', 'mails.mail_text', 'inbox.is_unread']);
                
//                 $to = DB::table('inbox')
//                 ->where(['inbox.is_trashed'=>0, 'inbox.user_id'=>$currentUser->id])
//                 ->join('to','to.mail_id','=','inbox.mail_id')
//                 ->join('users','users.id','=','to.user_id')
//                 ->get(['users.email']);
                
//                 $allMails['to'] = $to;
    
                return response()->json($allMails);
                
            } else {
                
                $errors = $validator->errors();
                return response()->json($errors->all());
            }
        } catch (\Exception $ex) {
           
            return response()->json(array($ex->getMessage()));
        }
    }

    /**
     * Delete mail from inbox
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteMail(Request $request) {
        
        try {
            if(!empty($request->id)) {
                
                $response = Inbox::where('id', $request->id)->update(['is_trashed'=>1]);
                
                $currentUser = User::where('email', $request->email)->first();
                
                $allMails = DB::table('inbox')
                ->where(['inbox.is_trashed'=>0, 'inbox.user_id'=>$currentUser->id])
                ->join('mails','mails.id','=','inbox.mail_id')
                ->join('users as u1','u1.id','=','inbox.user_id')
                ->join('users as u2','u2.id','=','mails.created_by')
                ->get(['u1.email as to', 'u2.email as from', 'inbox.id', 'mails.subject', 'mails.mail_text', 'inbox.is_unread']);
                
                return response()->json($allMails);
                
            } else {
                
                return response()->json(array('Id is not defined!'));
            }
        } catch (\Exception $ex) {
            
            return response()->json(array($ex->getMessage()));
        }
    }
    
    public function getMail(Request $request) {
        
        try {
            /**
             * Validate request for mandatory parameters
             * @var array $rules
             */
            $rules = array(
                'email' => 'required'
            );
            
            $messages = array(
                'email.required' => 'Your email address is required.'
            );
            
            $validator = \Validator::make(array(
                'email' => $request['email']
            ), $rules, $messages);
            
            if (!$validator->fails()) {
                
                /**
                 * Get current user details
                 * @var User $currentUser
                 */
                $currentUser = User::where('email', $request->email)->first();
                if (!$currentUser) {
                    
                    return response()->json(array('Invalid loggedin user.'));
                }
                
                $response = Inbox::where('id', $request->id)->update(['is_unread'=>0]);
                
                if ($response) {
                    
                    /**
                     * Get all mails from inbox for current user
                     * @var Inbox $allMails
                     */                
                    $mail = DB::table('inbox')
                    ->where(['inbox.id'=>$request->id, 'inbox.user_id'=>$currentUser->id])
                    ->join('mails','mails.id','=','inbox.mail_id')
                    ->join('users as u1','u1.id','=','inbox.user_id')
                    ->join('users as u2','u2.id','=','mails.created_by')
                    ->get(['u1.email as to', 'u2.email as from', 'inbox.id', 'mails.subject', 'mails.mail_text', 'inbox.is_unread']);
    
                    $tos = DB::table('to')
                    ->where(['to.mail_id'=>$mail[0]->id])
                    ->join('users','users.id','=','to.user_id')
                    ->get(['users.email']);

                    $tempTos = "";
                    foreach ($tos as $to) {
                        $tempTos .= $to->email . ';';
                    }
                    $mail[0]->to = substr_replace($tempTos, "", strlen($tempTos) - 1, 1) ;
                    $mail[0]->type = "inbox";
                    
                    return response()->json($mail);
                }
                
            } else {
                
                $errors = $validator->errors();
                return response()->json($errors->all());
            }
        } catch (\Exception $ex) {
            
            return response()->json(array($ex->getMessage()));
        }
    }
}
