<?php

namespace App\Http\Controllers;

use App\Drafts;
use App\Mails;
use App\To;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DraftsController extends Controller
{
    public function getAllMails(Request $request) {
        
        try {
            /**
             * Validate request for mandatory parameters
             * @var array $rules
             */
            $rules = array(
                'email' => 'required'
            );
            
            $messages = array(
                'email.required' => 'Your email address is required.'
            );
            
            $validator = \Validator::make(array(
                'email' => $request['email']
            ), $rules, $messages);
            
            if (!$validator->fails()) {
                
                /**
                 * Get current user details
                 * @var User $currentUser
                 */
                $currentUser = User::where('email', $request->email)->first();
                if (!$currentUser) {
                    
                    return response()->json(array('Invalid loggedin user.'));
                }
                
                /**
                 * Get all mails from drafts for current user
                 * @var Drafts $allMails
                 */
//                 $allMails = Drafts::where('user_id', $currentUser->id)
//                 ->where('is_trashed', 0)->with('mails')->with('to')->get();
                
                $allMails = DB::table('drafts')
                ->where(['drafts.is_trashed'=>0, 'drafts.user_id'=>$currentUser->id])
                ->join('mails','mails.id','=','drafts.mail_id')
                ->join('users as u1','u1.id','=','drafts.to_user_id')
                ->join('users as u2','u2.id','=','drafts.user_id')
                ->get(['u1.email as to', 'u2.email as from', 'drafts.id', 'mails.subject', 'mails.mail_text', 'drafts.is_unread']);
                
                return response()->json($allMails);
                
            } else {
                
                $errors = $validator->errors();
                return response()->json($errors->all());
            }
        } catch (\Exception $ex) {
            
            return response()->json(array($ex->getMessage()));
        }
    }
    
    /**
     * Delete mail from drafts
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteMail(Request $request) {
        
        try {
            if(!empty($request->id)) {
                
                $response = Drafts::where('id', $request->id)->update(['is_trashed'=>1]);
                
                $currentUser = User::where('email', $request->email)->first();
                
                $allMails = DB::table('drafts')
                ->where(['drafts.is_trashed'=>0, 'drafts.user_id'=>$currentUser->id])
                ->join('mails','mails.id','=','drafts.mail_id')
                ->join('users as u1','u1.id','=','drafts.to_user_id')
                ->join('users as u2','u2.id','=','drafts.user_id')
                ->get(['u1.email as to', 'u2.email as from', 'mails.id', 'mails.subject', 'mails.mail_text', 'drafts.is_unread']);
                
                return response()->json($allMails);
                
            } else {
                
                return response()->json(array('Id is not defined!'));
            }
        } catch (\Exception $ex) {
            
            return response()->json(array($ex->getMessage()));
        }
    }
    
    public function getMail(Request $request) {
        
        try {
            /**
             * Validate request for mandatory parameters
             * @var array $rules
             */
            $rules = array(
                'email' => 'required'
            );
            
            $messages = array(
                'email.required' => 'Your email address is required.'
            );
            
            $validator = \Validator::make(array(
                'email' => $request['email']
            ), $rules, $messages);
            
            if (!$validator->fails()) {
                
                /**
                 * Get current user details
                 * @var User $currentUser
                 */
                $currentUser = User::where('email', $request->email)->first();
                if (!$currentUser) {
                    
                    return response()->json(array('Invalid loggedin user.'));
                }
                
                $response = Drafts::where('id', $request->id)->update(['is_unread'=>0]);
                
                if ($response) {
                    /**
                     * Get all mails from drafts for current user
                     * @var Drafts $allMails
                     */
                    $mail = DB::table('drafts')
                    ->where(['drafts.id'=>$request->id, 'drafts.user_id'=>$currentUser->id])
                    ->join('mails','mails.id','=','drafts.mail_id')
                    ->join('users as u1','u1.id','=','drafts.to_user_id')
                    ->join('users as u2','u2.id','=','drafts.user_id')
                    ->get(['u1.email as to', 'u2.email as from', 'drafts.id', 'mails.subject', 'mails.mail_text', 'drafts.is_unread']);
                    
                    $tos = DB::table('to')
                    ->where(['to.mail_id'=>$mail[0]->id])
                    ->join('users','users.id','=','to.user_id')
                    ->get(['users.email']);
                    
                    $tempTos = "";
                    foreach ($tos as $to) {
                        $tempTos .= $to->email . ';';
                    }
                    $mail[0]->to = substr_replace($tempTos, "", strlen($tempTos) - 1, 1) ;
                    $mail[0]->type = "draft";
                    
                    return response()->json($mail);
                }
                
            } else {
                
                $errors = $validator->errors();
                return response()->json($errors->all());
            }
        } catch (\Exception $ex) {
            
            return response()->json(array($ex->getMessage()));
        }
    }
    
    public function save(Request $request)
    {
        try {
            $rules = array(
                'to' => 'required'
            );
            
            $messages = array(
                'to.required' => 'recipient email address is required.'
            );
            
            $validator = \Validator::make(array(
                'to' => $request['to']
            ), $rules, $messages);
            
            if (!$validator->fails()) {
                
                $currentUser = User::where('email', $request->from)->first();
                
                $emails = explode(';', $request['to']);
                
                foreach ($emails as $email) {
                    
                    $rules = array(
                        'email' => 'exists:users,email'
                    );
                    
                    $messages = array(
                        'exists' => $email . ' email address does not exist.'
                    );
                    
                    $validator = \Validator::make(array(
                        'email' => $email
                    ), $rules, $messages);
                    
                    if ($validator->fails()) {
                        
                        $errors = $validator->errors();
                        return response()->json($errors->all());
                    }
                }
                
                $createdMail = Mails::create(array(
                    'subject' => $request['subject'],
                    'mail_text' => $request['mail_text'],
                    'created_by' => $currentUser->id
                ));
                
                if ($createdMail) {
                    
                    foreach ($emails as $email) {
                        
                        $user = User::where('email', trim(str_replace(' ', '', $email)))->first();
                        $draftsEntry = Drafts::create(array(
                            'user_id' => $currentUser->id,
                            'to_user_id' => $user->id,
                            'mail_id' => $createdMail->id,
                            'is_trashed' => 0,
                            'is_unread' => 1
                        ));
                        
                        if ($draftsEntry) {
                                                        
                            $toEntry = To::create(array(
                                'user_id' => $user->id,
                                'mail_id' => $createdMail->id
                            ));
                        }
                    }
                    return response()->json(array('Saved as draft'));
                }
            }
        } catch (\Exception $ex) {
            return response()->json(array($ex->getMessage()));
        }
    }
}
