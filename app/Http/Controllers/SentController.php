<?php

namespace App\Http\Controllers;

use App\Sent;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SentController extends Controller
{
    public function getAllMails(Request $request) {
        
        try {
            /**
             * Validate request for mandatory parameters
             * @var array $rules
             */
            $rules = array(
                'email' => 'required'
            );
            
            $messages = array(
                'email.required' => 'Your email address is required.'
            );
            
            $validator = \Validator::make(array(
                'email' => $request['email']
            ), $rules, $messages);
            
            if (!$validator->fails()) {
                
                /**
                 * Get current user details
                 * @var User $currentUser
                 */
                $currentUser = User::where('email', $request->email)->first();
                if (!$currentUser) {
                    
                    return response()->json(array('Invalid loggedin user.'));
                }
                
                /**
                 * Get all mails from drafts for current user
                 * @var Sent $allMails
                 */
//                 $allMails = Sent::where('user_id', $currentUser->id)
//                 ->where('is_trashed', 0)->with('mails')->with('to')->get();
                
                $allMails = DB::table('sent')
                ->where(['sent.is_trashed'=>0, 'sent.user_id'=>$currentUser->id])
                ->join('mails','mails.id','=','sent.mail_id')
                ->join('users as u1','u1.id','=','sent.to_user_id')
                ->join('users as u2','u2.id','=','sent.user_id')
                ->get(['u1.email as to', 'u2.email as from', 'sent.id', 'mails.subject', 'mails.mail_text', 'sent.is_unread']);
                
                return response()->json($allMails);
                
            } else {
                
                $errors = $validator->errors();
                return response()->json($errors->all());
            }
        } catch (\Exception $ex) {
            
            return response()->json(array($ex->getMessage()));
        }
    }
    
    /**
     * Delete mail from sent
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteMail(Request $request) {
        
        try {
            
            if(!empty($request->id)) {
                
                $response = Sent::where('id', $request->id)->update(['is_trashed'=>1]);
                
                $currentUser = User::where('email', $request->email)->first();
                
                $allMails = DB::table('sent')
                ->where(['sent.is_trashed'=>0, 'sent.user_id'=>$currentUser->id])
                ->join('mails','mails.id','=','sent.mail_id')
                ->join('users as u1','u1.id','=','sent.to_user_id')
                ->join('users as u2','u2.id','=','sent.user_id')
                ->get(['u1.email as to', 'u2.email as from', 'mails.id', 'mails.subject', 'mails.mail_text', 'sent.is_unread']);
                
                return response()->json($allMails);
                
            } else {
                
                return response()->json(array('Id is not defined!'));
            }
        } catch (\Exception $ex) {
            
            return response()->json(array($ex->getMessage()));
        }
    }
    
    public function getMail(Request $request) {
        
        try {
            /**
             * Validate request for mandatory parameters
             * @var array $rules
             */
            $rules = array(
                'email' => 'required'
            );
            
            $messages = array(
                'email.required' => 'Your email address is required.'
            );
            
            $validator = \Validator::make(array(
                'email' => $request['email']
            ), $rules, $messages);
            
            if (!$validator->fails()) {
                
                /**
                 * Get current user details
                 * @var User $currentUser
                 */
                $currentUser = User::where('email', $request->email)->first();
                if (!$currentUser) {
                    
                    return response()->json(array('Invalid loggedin user.'));
                }
                
                $response = Sent::where('id', $request->id)->update(['is_unread'=>0]);
                
                if ($response) {
                
                    /**
                     * Get all mails from drafts for current user
                     * @var Sent $allMails
                     */
                    $mail = DB::table('sent')
                    ->where(['sent.id'=>$request->id, 'sent.user_id'=>$currentUser->id])
                    ->join('mails','mails.id','=','sent.mail_id')
                    ->join('users as u1','u1.id','=','sent.to_user_id')
                    ->join('users as u2','u2.id','=','sent.user_id')
                    ->get(['u1.email as to', 'u2.email as from', 'sent.id', 'mails.subject', 'mails.mail_text', 'sent.is_unread']);
                    
                    $tos = DB::table('to')
                    ->where(['to.mail_id'=>$mail[0]->id])
                    ->join('users','users.id','=','to.user_id')
                    ->get(['users.email']);
                    
                    $tempTos = "";
                    foreach ($tos as $to) {
                        $tempTos .= $to->email . ';';
                    }
                    
                    $mail[0]->to = substr_replace($tempTos, "", strlen($tempTos) - 1, 1) ;
                    $mail[0]->type = "sent";
                    
                    return response()->json($mail);
                }
            } else {
                
                $errors = $validator->errors();
                return response()->json($errors->all());
            }
        } catch (\Exception $ex) {
            
            return response()->json(array($ex->getMessage()));
        }
    }
}
