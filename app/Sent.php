<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sent extends Model
{
    protected $fillable = ['user_id', 'to_user_id', 'mail_id', 'is_unread', 'is_trashed'];
    protected $table = 'sent';
    
    public function mails() {
        return $this->hasMany('App\Mails', 'id', 'mail_id');
    }
    
    public function to() {
        return $this->hasMany('App\User', 'id', 'to_user_id');
    }
    
    public function from() {
        return $this->hasMany('App\User', 'id', 'user_id');
    }
}
