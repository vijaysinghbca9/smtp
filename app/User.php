<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function InboxDetails()
    {
        return $this->hasMany('App\Inbox', 'user_id', 'id');
    }
    
    public function SentDetails()
    {
        return $this->hasMany('App\Sent', 'user_id', 'id');
    }
    
    public function DraftsDetails()
    {
        return $this->hasMany('App\Drafts', 'user_id', 'id');
    }
    
    public function TrashDetails()
    {
        return $this->hasMany('App\Trash', 'user_id', 'id');
    }
}
