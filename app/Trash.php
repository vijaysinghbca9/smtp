<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trash extends Model
{
    protected $table = 'mails';
    
    public function trashedInboxMails() {
        return $this->hasMany('App\Inbox', 'mail_id', 'id');
    }
    
    public function trashedSentMails() {
        return $this->hasMany('App\Sent', 'mail_id', 'id');
    }
    
//     public function to() {
//         return $this->hasMany('App\User', 'id', 'user_id');
//     }
}
