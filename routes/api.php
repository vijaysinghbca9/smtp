<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/inbox/{email}', 'InboxController@getAllInboxMails');
Route::get('/sent/{email}', 'SentController@getAllSentMails');
Route::get('/draft/{email}', 'DraftsController@getAllDraftsMails');
Route::get('/trash/{email}', 'TrashController@getAllTrashMails');

Route::post('/send', 'ComposeMailController@sendMail');
Route::post('/inbox', 'InboxController@getAllMails');
Route::post('/drafts', 'DraftsController@getAllMails');
Route::post('/sent', 'SentController@getAllMails');
Route::post('/trash', 'TrashController@getAllMails');

Route::post('/inbox/get', 'InboxController@getMail');
Route::post('/sent/get', 'SentController@getMail');
Route::post('/drafts/get', 'DraftsController@getMail');
Route::post('/trash/inbox/get', 'TrashController@getInboxMail');
Route::post('/trash/sent/get', 'TrashController@getSentMail');

Route::post('/inbox/delete', 'InboxController@deleteMail');
Route::post('/drafts/delete', 'DraftsController@deleteMail');
Route::post('/sent/delete', 'SentController@deleteMail');
Route::post('/trash/delete', 'TrashController@deleteMail');
Route::post('/drafts/save', 'DraftsController@save');