<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sent', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->index();
            $table->bigInteger('to_user_id');
            $table->bigInteger('mail_id');
            //$table->string('subject')->nullable();
            //$table->longText('mail_text')->nullable();
            $table->boolean('is_unread');
            $table->boolean('is_trashed')->index();
            $table->timestamps();
        });
        
        Schema::table('sent', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('mail_id')->references('id')->on('mails');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sent');
    }
}
