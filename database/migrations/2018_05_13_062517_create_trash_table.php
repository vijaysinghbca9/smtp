<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrashTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trash', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->index();
            $table->bigInteger('from_user_id');
            $table->bigInteger('mail_id');
            //$table->string('subject')->nullable();
            //$table->longText('mail_text')->nullable();
            $table->boolean('is_unread');
            $table->timestamps();
        });
            
        Schema::table('trash', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('mail_id')->references('id')->on('mails');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trash');
    }
}
