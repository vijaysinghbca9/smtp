-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 18, 2018 at 06:54 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smtp`
--

-- --------------------------------------------------------

--
-- Table structure for table `drafts`
--

DROP TABLE IF EXISTS `drafts`;
CREATE TABLE IF NOT EXISTS `drafts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `to_user_id` bigint(20) NOT NULL,
  `mail_id` bigint(20) NOT NULL,
  `is_unread` tinyint(1) NOT NULL,
  `is_trashed` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `drafts_user_id_index` (`user_id`),
  KEY `drafts_is_trashed_index` (`is_trashed`),
  KEY `drafts_mail_id_foreign` (`mail_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `drafts`
--

INSERT INTO `drafts` (`id`, `user_id`, `to_user_id`, `mail_id`, `is_unread`, `is_trashed`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 18, 0, 1, '2018-05-17 19:19:39', '2018-05-17 19:24:46'),
(2, 1, 1, 19, 0, 0, '2018-05-17 19:20:59', '2018-05-17 19:38:52'),
(3, 1, 1, 22, 1, 0, '2018-05-17 19:41:25', '2018-05-17 19:41:25');

-- --------------------------------------------------------

--
-- Table structure for table `inbox`
--

DROP TABLE IF EXISTS `inbox`;
CREATE TABLE IF NOT EXISTS `inbox` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `from_user_id` bigint(20) NOT NULL,
  `mail_id` bigint(20) NOT NULL,
  `is_unread` tinyint(1) NOT NULL,
  `is_trashed` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `inbox_user_id_index` (`user_id`),
  KEY `inbox_is_trashed_index` (`is_trashed`),
  KEY `inbox_mail_id_foreign` (`mail_id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inbox`
--

INSERT INTO `inbox` (`id`, `user_id`, `from_user_id`, `mail_id`, `is_unread`, `is_trashed`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 1, 0, 1, '2018-05-16 17:18:46', '2018-05-17 11:43:22'),
(2, 2, 1, 2, 1, 0, '2018-05-16 17:19:37', '2018-05-16 17:19:37'),
(3, 1, 1, 3, 1, 1, '2018-05-16 17:28:56', '2018-05-16 19:22:44'),
(4, 1, 1, 4, 0, 1, '2018-05-16 19:25:31', '2018-05-17 11:43:33'),
(5, 1, 1, 5, 1, 1, '2018-05-16 19:28:22', '2018-05-16 19:28:25'),
(6, 1, 1, 6, 1, 1, '2018-05-16 19:31:45', '2018-05-16 19:33:04'),
(7, 1, 1, 7, 1, 1, '2018-05-16 19:33:18', '2018-05-16 19:33:21'),
(9, 1, 1, 9, 0, 0, '2018-05-16 20:23:02', '2018-05-17 15:49:44'),
(10, 1, 1, 10, 0, 0, '2018-05-17 07:15:01', '2018-05-17 15:56:44'),
(11, 2, 1, 10, 0, 0, '2018-05-17 07:15:01', '2018-05-17 15:37:12'),
(12, 1, 1, 11, 0, 0, '2018-05-17 15:37:06', '2018-05-17 16:04:25'),
(13, 1, 1, 12, 0, 0, '2018-05-17 15:50:01', '2018-05-17 18:45:47'),
(14, 1, 1, 13, 0, 0, '2018-05-17 15:57:15', '2018-05-17 18:45:55'),
(15, 1, 1, 14, 0, 0, '2018-05-17 16:21:04', '2018-05-17 18:16:12'),
(16, 3, 1, 15, 1, 0, '2018-05-17 18:16:52', '2018-05-17 18:16:52'),
(17, 1, 1, 15, 0, 1, '2018-05-17 18:16:52', '2018-05-17 18:38:08'),
(18, 1, 1, 16, 1, 0, '2018-05-17 18:31:54', '2018-05-17 18:31:54'),
(19, 3, 1, 16, 1, 0, '2018-05-17 18:31:54', '2018-05-17 18:31:54'),
(20, 2, 1, 20, 1, 0, '2018-05-17 19:40:18', '2018-05-17 19:40:18'),
(21, 1, 1, 20, 0, 0, '2018-05-17 19:40:18', '2018-05-17 19:40:24'),
(22, 2, 1, 21, 1, 0, '2018-05-17 19:40:35', '2018-05-17 19:40:35');

-- --------------------------------------------------------

--
-- Table structure for table `mails`
--

DROP TABLE IF EXISTS `mails`;
CREATE TABLE IF NOT EXISTS `mails` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail_text` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mails`
--

INSERT INTO `mails` (`id`, `created_by`, `subject`, `mail_text`, `created_at`, `updated_at`) VALUES
(1, 2, 'test mail', 'mail from avi to vijay', '2018-05-16 17:18:46', '2018-05-16 17:18:46'),
(2, 1, 'test mail', 'mail from vijay to avi and himself.', '2018-05-16 17:19:37', '2018-05-16 17:19:37'),
(3, 1, 'test mail', 'mail from vijay to vijay', '2018-05-16 17:28:56', '2018-05-16 17:28:56'),
(4, 1, 'test mail', 'test mail from vijay vijay :)', '2018-05-16 19:25:31', '2018-05-16 19:25:31'),
(5, 1, 'test mail', 'Hi', '2018-05-16 19:28:22', '2018-05-16 19:28:22'),
(6, 1, 'test', 'test', '2018-05-16 19:31:45', '2018-05-16 19:31:45'),
(7, 1, 'test', 'tesssss', '2018-05-16 19:33:18', '2018-05-16 19:33:18'),
(8, 1, 'test', 'test mail...', '2018-05-16 19:43:10', '2018-05-16 19:43:10'),
(9, 1, 'test mail test', 'test mail busy', '2018-05-16 20:23:02', '2018-05-16 20:23:02'),
(10, 1, 'test mail to both users', 'Hi, it\'s a test mail.', '2018-05-17 07:15:01', '2018-05-17 07:15:01'),
(11, 1, 'Fwd: test mail test', NULL, '2018-05-17 15:37:06', '2018-05-17 15:37:06'),
(12, 1, 'Fwd: test mail test', 'Hi Vijay\r\n\r\nSubject: test mail test\r\nTo: vijay@gmail.com\r\n\r\ntest mail busy', '2018-05-17 15:50:01', '2018-05-17 15:50:01'),
(13, 1, 'Fwd: test mail to both users', 'Subject: test mail to both users\r\nTo: vijay@gmail.com;avi@gmail.com\r\n\r\nHi, it\'s a test mail.', '2018-05-17 15:57:15', '2018-05-17 15:57:15'),
(14, 1, 'Re: test', 'Subject: test\r\nTo: vijay@gmail.com\r\n\r\ntest mail...', '2018-05-17 16:21:04', '2018-05-17 16:21:04'),
(15, 1, 'Fwd: Re: test', 'Subject: Re: test\r\nTo: \r\n\r\nSubject: test&#13;To: vijay@gmail.com&#13;&#13;test mail...', '2018-05-17 18:16:52', '2018-05-17 18:16:52'),
(16, 1, 'Re: test mail to both users', 'Subject: test mail to both users\r\nTo: vijay@gmail.com\r\n\r\nHi, it\'s a test mail.', '2018-05-17 18:31:54', '2018-05-17 18:31:54'),
(17, 1, 'test', 'test', '2018-05-17 19:11:01', '2018-05-17 19:11:01'),
(18, 1, 'Test mail', 'test', '2018-05-17 19:19:39', '2018-05-17 19:19:39'),
(19, 1, 'Test mail', 'test', '2018-05-17 19:20:59', '2018-05-17 19:20:59'),
(20, 1, 'Test mail', 'test...', '2018-05-17 19:40:18', '2018-05-17 19:40:18'),
(21, 1, 'Fwd: Test mail', 'Subject: Test mail\r\nTo: \r\n\r\ntest...', '2018-05-17 19:40:35', '2018-05-17 19:40:35'),
(22, 1, 'Test Draft', 'It is a test draft.', '2018-05-17 19:41:25', '2018-05-17 19:41:25');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_05_13_060059_create_mails_table', 1),
(4, '2018_05_13_062326_create_inbox_table', 1),
(5, '2018_05_13_062348_create_sent_table', 1),
(6, '2018_05_13_062404_create_drafts_table', 1),
(7, '2018_05_13_062517_create_trash_table', 1),
(8, '2018_05_13_080842_create_to_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sent`
--

DROP TABLE IF EXISTS `sent`;
CREATE TABLE IF NOT EXISTS `sent` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `to_user_id` bigint(20) NOT NULL,
  `mail_id` bigint(20) NOT NULL,
  `is_unread` tinyint(1) NOT NULL,
  `is_trashed` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sent_user_id_index` (`user_id`),
  KEY `sent_is_trashed_index` (`is_trashed`),
  KEY `sent_mail_id_foreign` (`mail_id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sent`
--

INSERT INTO `sent` (`id`, `user_id`, `to_user_id`, `mail_id`, `is_unread`, `is_trashed`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 1, 0, 0, '2018-05-16 17:18:46', '2018-05-16 17:18:46'),
(2, 1, 2, 2, 0, 0, '2018-05-16 17:19:37', '2018-05-16 17:19:37'),
(3, 1, 1, 3, 0, 1, '2018-05-16 17:28:56', '2018-05-16 17:28:56'),
(4, 1, 1, 4, 0, 0, '2018-05-16 19:25:31', '2018-05-16 19:25:31'),
(5, 1, 1, 5, 0, 1, '2018-05-16 19:28:22', '2018-05-16 19:59:27'),
(6, 1, 1, 6, 0, 1, '2018-05-16 19:31:45', '2018-05-16 19:59:12'),
(7, 1, 1, 7, 0, 1, '2018-05-16 19:33:18', '2018-05-17 11:43:12'),
(8, 1, 1, 8, 0, 1, '2018-05-16 19:43:10', '2018-05-17 16:20:52'),
(9, 1, 1, 9, 0, 1, '2018-05-16 20:23:02', '2018-05-17 11:43:07'),
(10, 1, 1, 10, 0, 1, '2018-05-17 07:15:01', '2018-05-17 18:17:22'),
(11, 1, 2, 10, 0, 0, '2018-05-17 07:15:01', '2018-05-17 18:31:20'),
(12, 1, 1, 11, 0, 1, '2018-05-17 15:37:06', '2018-05-17 18:17:18'),
(13, 1, 1, 12, 0, 0, '2018-05-17 15:50:01', '2018-05-17 16:20:02'),
(14, 1, 1, 13, 0, 0, '2018-05-17 15:57:15', '2018-05-17 16:15:04'),
(15, 1, 1, 14, 0, 0, '2018-05-17 16:21:04', '2018-05-17 16:21:04'),
(16, 1, 3, 15, 0, 0, '2018-05-17 18:16:52', '2018-05-17 18:16:52'),
(17, 1, 1, 15, 0, 0, '2018-05-17 18:16:52', '2018-05-17 18:17:07'),
(18, 1, 1, 16, 0, 0, '2018-05-17 18:31:54', '2018-05-17 18:31:54'),
(19, 1, 3, 16, 0, 0, '2018-05-17 18:31:54', '2018-05-17 18:31:54'),
(20, 1, 2, 20, 0, 0, '2018-05-17 19:40:18', '2018-05-17 19:40:18'),
(21, 1, 1, 20, 0, 0, '2018-05-17 19:40:18', '2018-05-17 19:40:18'),
(22, 1, 2, 21, 0, 0, '2018-05-17 19:40:35', '2018-05-17 19:40:35');

-- --------------------------------------------------------

--
-- Table structure for table `to`
--

DROP TABLE IF EXISTS `to`;
CREATE TABLE IF NOT EXISTS `to` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `mail_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `to_user_id_foreign` (`user_id`),
  KEY `to_mail_id_foreign` (`mail_id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `to`
--

INSERT INTO `to` (`id`, `user_id`, `mail_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-05-16 17:18:46', '2018-05-16 17:18:46'),
(2, 2, 2, '2018-05-16 17:19:37', '2018-05-16 17:19:37'),
(3, 1, 3, '2018-05-16 17:28:56', '2018-05-16 17:28:56'),
(4, 1, 4, '2018-05-16 19:25:31', '2018-05-16 19:25:31'),
(5, 1, 5, '2018-05-16 19:28:22', '2018-05-16 19:28:22'),
(6, 1, 6, '2018-05-16 19:31:45', '2018-05-16 19:31:45'),
(7, 1, 7, '2018-05-16 19:33:18', '2018-05-16 19:33:18'),
(8, 1, 8, '2018-05-16 19:43:10', '2018-05-16 19:43:10'),
(9, 1, 9, '2018-05-16 20:23:02', '2018-05-16 20:23:02'),
(10, 1, 10, '2018-05-17 07:15:01', '2018-05-17 07:15:01'),
(11, 2, 10, '2018-05-17 07:15:01', '2018-05-17 07:15:01'),
(12, 1, 11, '2018-05-17 15:37:06', '2018-05-17 15:37:06'),
(13, 1, 12, '2018-05-17 15:50:01', '2018-05-17 15:50:01'),
(14, 1, 13, '2018-05-17 15:57:15', '2018-05-17 15:57:15'),
(15, 1, 14, '2018-05-17 16:21:04', '2018-05-17 16:21:04'),
(16, 3, 15, '2018-05-17 18:16:52', '2018-05-17 18:16:52'),
(17, 1, 15, '2018-05-17 18:16:52', '2018-05-17 18:16:52'),
(18, 1, 16, '2018-05-17 18:31:54', '2018-05-17 18:31:54'),
(19, 3, 16, '2018-05-17 18:31:54', '2018-05-17 18:31:54'),
(20, 1, 19, '2018-05-17 19:20:59', '2018-05-17 19:20:59'),
(21, 2, 20, '2018-05-17 19:40:18', '2018-05-17 19:40:18'),
(22, 1, 20, '2018-05-17 19:40:18', '2018-05-17 19:40:18'),
(23, 2, 21, '2018-05-17 19:40:35', '2018-05-17 19:40:35'),
(24, 1, 22, '2018-05-17 19:41:25', '2018-05-17 19:41:25');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Vijay', 'vijay@gmail.com', '$2y$10$rM0IYFSqMPVDmrO80DOJWeI7M1TUCQX68bJX0HJuvxCo4M9kru9LS', 'QriX6y1XwT3f3MhxYZd5u994AGSn6bVjkRzhXKHZCW2Jgr96mMO1uWYLG6e2', '2018-05-16 17:17:31', '2018-05-16 17:17:31'),
(2, 'Avinash', 'avi@gmail.com', '$2y$10$S1hDg0ObT9SIAj5sZSEPbelUfoKdrJiwSZ21/88oKtbAzN/GiGqsO', 'VKb77jpA0dQLWWucZdutk08mUDxzrSkuv3d8oQAzUnxaxavpXElLWzh2OXdI', '2018-05-16 17:18:00', '2018-05-16 17:18:00'),
(3, 'Abhi', 'abhi@gmail.com', '$2y$10$jS5nd1QkbK9LCalO5uur.eXaJB7fAIEEkxCTOvUgsx/o6Zt3c4FEe', 'jME8YsZ5XTbBNdIL9x6oPpjX9OnEROdGvJ3Au14F6424bPEV4WDkFsD1wMJS', '2018-05-17 18:10:51', '2018-05-17 18:10:51'),
(4, 'Anand', 'anand@gmail.com', '$2y$10$KImm7kJqokefrxWKtYcGh.pB1CJITi0lVlbR8tsS6HrFBDE05lEu.', 'mkNkIwhXlzieo4cXJjurdXGysFZpSgcMnNY75oMOuBLt6B3EaUflyswb8Wtx', '2018-05-17 18:15:40', '2018-05-17 18:15:40'),
(5, 'Ashu', 'ashu@gmail.com', '$2y$10$bpOaVAjjCnY46AO5ZG52Iu7cCkJbQzISmiK/nlgJB6nR6OUqy1Qoi', 'cJvaNshfDkxpuQJxhbvJwpr9fr9iEFFkRQrJqKfZOkC5lEDfMsRqjPDs2WV5', '2018-05-17 19:59:57', '2018-05-17 19:59:57'),
(6, 'Ankur', 'ankur@gmail.com', '$2y$10$pHIxpd//rX8y/be98G5TjOhFGHeGx94SgPzUF1aoiffAdYk8.4tX2', 's8YjNgi3kqX8xTKkXWBtWHHhYx4yiDQOjDdr7NSGPQ6WOqHaI5nBBcmXNUjK', '2018-05-17 20:01:19', '2018-05-17 20:01:19'),
(7, 'Temp', 'temp@gmail.com', '$2y$10$/0iH5XQclFHC/s9TLi03jeuE/LTI8GfMNrQNNZWHv0b0lHXytqowG', '306DIxMm1h8lYVLJgvgTJVNCnS7W2HXmcMfAFPrZTart8rR0UEzCpONgS3XU', '2018-05-17 20:23:09', '2018-05-17 20:23:09');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
